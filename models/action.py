class Action(object):
    def __init__(self, name, value=None, variables=None, set_variable=None, get_variables=None):
        self.name = name
        self.value = value
        self.variables = variables
        self.set_variable = set_variable
        self.get_variables = get_variables
