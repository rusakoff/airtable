from actions import ActionsList 

actions_list = {
    "a_initialize": ActionsList.a_initialize,
    "a_language_set": ActionsList.a_language_set,
    "a_message_ura": ActionsList.a_message_ura,
    "a_message_how_instructions": ActionsList.a_message_how_instructions,
    "a_language_select": ActionsList.a_language_select,
    "a_registration_email_enter": ActionsList.a_registration_email_enter,
    "a_registration_email_check": ActionsList.a_registration_email_check,
    "a_registration_enter_email_code": ActionsList.a_registration_enter_email_code,
    "a_registration_email_check_code": ActionsList.a_registration_email_check_code,
    "a_gender_select": ActionsList.a_gender_select,
    "a_gender_set": ActionsList.a_gender_set,
    "a_telegram_start": ActionsList.a_telegram_start,
    "a_telegram_transfer": ActionsList.a_telegram_transfer,
    "a_telegram_transfer_yes": ActionsList.a_telegram_transfer_yes,
}