from data.db import DB
from models.action import Action
from models.task import Task

# Доступ к переменной ТекстФилда action.variables['value']
# Доступ к переменной get_variable action.variables['var_temp_email']

class ActionsList:
    def __init__(self, app):
        self.app = app
    # ----------------------------------------------------------- #
    # ИНИЦИАЛИЗАЦИЯ
    # ----------------------------------------------------------- #
    def a_initialize(self, action: Action, guid=None) -> Task:
        print(action.name)
        return self.a_language_select(action)

    # ----------------------------------------------------------- #
    # ЯЗЫК
    # ----------------------------------------------------------- #
    def a_language_select(self, action: Action, guid=None) -> Task:
        task = Task("t_language_select")
        return task


    def a_language_set(self, action: Action, guid=None) -> Task:
        # TODO: изменить язык в профиле
        task = Task("t_language_ok")
        return task

    # ----------------------------------------------------------- #
    # СООБЩЕНИЯ БЕЗ ГРУППЫ
    # ----------------------------------------------------------- #
    def a_message_ura(self, action: Action, guid=None) -> Task:
        task = Task("t_message_ura")
        return task

    def a_message_how_instructions(self, action: Action, guid=None) -> Task:
        task = Task("t_message_how_instructions")                                                           
        return task

    # ----------------------------------------------------------- #
    # РЕГИСТРАЦИЯ И ВЕРИФИКАЦИЯ ИМЕЙЛА
    # ----------------------------------------------------------- #
    def a_registration_email_enter(self, action: Action, guid=None) -> Task:
        task = Task("t_registration_email_enter") 
        return task

    def a_registration_email_check(self, action: Action, guid=None) -> Task:
        tf_value=action.variables['value']
        task = Task("t_registration_email_check")
        return task

    def a_registration_enter_email_code(self, action: Action, guid=None) -> Task:
        print(action.variables);
        # {'value': '', 'var_temp_email': 'rrrr@ttt.jj'}
        # TODO: сделать отпрваку имейла
        # И генерацию кода
        code = "123"
        task = Task("t_registration_enter_email_code")
        task.add_data("var_temp_email_code", code)
        return task

    def a_registration_email_check_code(self, action: Action, guid=None) -> Task:
        # TODO: здесь уже код подтвержден!
        task = Task("t_registration_email_final")
        
        # Ищем guid по email
        email = action.variables['var_temp_email']
        query = f"SELECT guid FROM profiles WHERE profile ->> 'email' = '{email}'"
        response = DB.query_select(query)
        # 
        # TODO: если не найден то нужно создать профиль и отправить guid!
        # 
        if any(response):
            resultJson = response[0][0]
            if resultJson!="" and resultJson!=None:
                task.add_data("var_guid", resultJson)
        else: 
            task.add_data("var_guid", "СЮДА НОВЫЙ GUID")

        return task

    # ----------------------------------------------------------- #
    # TELEGRAM
    # ----------------------------------------------------------- #
    def a_telegram_start(self, action: Action, guid=None) -> Task:
        # TODO: сделать отпрваку имейла
        task = Task("t_telegram_start")
        return task

    def a_telegram_have(self, action: Action, guid=None) -> Task:
        # TODO: сделать отпрваку имейла
        value = action.variables['value']
        if value == "true":
            task = Task("t_telegram_transfer")
            return task
        if value == "false":
            task = Task("t_telegram_good_day")
        return task

    def a_telegram_transfer(self, action: Action, guid=None) -> Task:
        # TODO: сделать отпрваку кода в телеграм
        # И генерацию здесь
        code = "123"
        value = action.variables['value']
        if value == "true":
            task = Task("t_telegram_enter_code")
            task.add_data("var_telegram_code", code)
        if value == "false":
            task = Task("t_telegram_later")
        return task

    def a_telegram_code_check(value, guid=None) -> Task:
        task = Task("t_telegram_check_code_ok")
        return task

    # ----------------------------------------------------------- #
    # ПОЛ и ОСТАЛЬНЫЕ
    # ----------------------------------------------------------- #
    def a_gender_select(self, action: Action, guid=None) -> Task:
        # TODO: сделать отпрваку имейла
        task = Task("t_gender_select")
        return task

    def a_gender_set(self, action: Action, guid=None) -> Task:
        # TODO: сделать сохранение пола в профиле
        task = Task("t_gender_set")
        value = action.variables['value']
        task.add_data("var_gender", value)
        return task
    
    def a_migren_aura(self, action: Action, guid=None) -> Task:
        value = action.variables['value']
        task = Task("t_migren_aura_select")
        task.add_data("var_migren_aura", value)
        return task

    def a_migren_aura_set(self, action: Action, guid=None) -> Task:
        value = action.variables['value']
        task = Task("t_migren_aura_final")
        task.add_data("var_migren_aura", value)
        return task

    def a_notification_enter_time(self, action: Action, guid=None) -> Task:
        # value = action.variables['value']
        task = Task("t_notification_enter_time")
        # task.add_data("var_notification_time", value)
        return task

    def a_notification_time_set(self, action: Action, guid=None) -> Task:
        value = action.variables['value']
        task = Task("t_notification_time_set")
        task.add_data("var_notification_time", value)
        return task
    
    # ----------------------------------------------------------- #
    #
    # К О М А Н Д Ы
    #
    # ----------------------------------------------------------- #
    def a_help_message(self, action: Action, guid=None) -> Task:
        task = Task("t_command_message_how_instruction")
        return task
    
    def a_command_yesterday(self, action: Action, guid=None) -> Task:
        task = Task("t_command_yesterday")
        return task

    def a_command_today(self, action: Action, guid=None) -> Task:
        task = Task("t_command_today")
        return task
    def a_command_custom(self, action: Action, guid=None) -> Task:
        task = Task("t_command_custom")
        return task
    def a_command_custom_set(self, action: Action, guid=None) -> Task:
        #TODO Сюда пришел кастом вопрос
        # print(action.variables['value'])
        task = Task("t_command_custom_set")
        return task

    def a_command_doctor(self, action: Action, guid=None) -> Task:
        task = Task("t_command_doctor")
        return task

    def a_command_doctor_email_set(self, action: Action, guid=None) -> Task:
        # email=action.variables['value']
        task = Task("t_command_doctor_your_name")
        return task
    def a_command_doctor_your_name_set(self, action: Action, guid=None) -> Task:
        # name=action.variables['value']
        task = Task("t_command_doctor_sended")
        return task

    def a_command_week(self, action: Action, guid=None) -> Task:
        task = Task("t_command_week")
        return task
        
    def a_command_month(self, action: Action, guid=None) -> Task:
        task = Task("t_command_month")
        return task

    def a_command_propustil(self, action: Action, guid=None) -> Task:
        task = Task("t_command_propustil")
        return task

    def a_command_when(self, action: Action, guid=None) -> Task:
        task = Task("t_notification_enter_time")
        return task

    def a_command_send(self, action: Action, guid=None) -> Task:
        task = Task("t_command_send")
        return task

    def a_command_note(self, action: Action, guid=None) -> Task:
        # var_mode_notes берется потому что указано в аиртейбле get_variables
        var_mode_notes = action.variables['var_mode_notes'] if 'var_mode_notes' in action.variables else "false"
        if var_mode_notes == "false":
            task = Task("t_command_note_on")
            task.add_data("var_mode_notes", "true")
        else: 
            task = Task("t_command_note_off")
            task.add_data("var_mode_notes", "false")
        return task
    
    def a_command_note_save(self, action: Action, guid=None) -> Task:
        task = Task("t_note_saved")
        return task

    def a_command_propustil_set(self, action: Action, guid=None) -> Task:
        if (action.variables['value'] != ""):
            task = Task("t_quiz_u_vas_bolela_golova_date")
        else:    
            #TODO: Нужна проверка про вчерашний день
            # ЕСЛИ Вчера включено то 
            # task = t_quiz_u_vas_bolela_golova_yesterday
            task = Task("t_quiz_u_vas_bolela_golova")
        return task

    def a_command_iamdoctor(self, action: Action, guid=None) -> Task:
        task = Task("t_command_iamdoctor")
        return task
    # ----------------------------------------------------------- #
    #
    #  О П Р О С
    #
    # ----------------------------------------------------------- #
    def a_quiz_start(self, action: Action, guid=None) -> Task:
        if (action.variables['value'] != ""):
            task = Task("t_quiz_u_vas_bolela_golova_date")
        else:    
            #TODO: Нужна проверка про вчерашний день
            # ЕСЛИ Вчера включено то 
            # task = t_quiz_u_vas_bolela_golova_yesterday
            task = Task("t_quiz_u_vas_bolela_golova")
        return task

    def a_quiz_prinimal_obezbol(self, action: Action, guid=None) -> Task:
        task = Task("t_quiz_kakoy_preparat")
        # Примеры добаления в список
        task.links.append({"text":"Анальгин", "value":"analgin", "actions":[{"name":"a_quiz_pomoglo"}]})
        task.links.append({"text":"Парацетамол", "value":"paracetamol", "actions":[{"name":"a_quiz_pomoglo"}]})
        task.links.append({"text":"+", "value":"", "actions":[{"name":"a_quiz_obezbol_add"}]})
        return task
    
    def a_quiz_obezbol_add(self, action: Action, guid=None) -> Task:
        print(action.variables['value'])
        #TODO здесь выбранный препарат
        task = Task("t_quiz_obezbol_add")
        return task

    def a_quiz_obezbol_add_set(self, action: Action, guid=None) -> Task:
        print(action.variables['value'])
        #TODO здесь добавленный препарат появился, надо добавить по guid
        task = Task("t_quiz_pomoglo")
        return task

    def a_quiz_pomoglo(self, action: Action, guid=None) -> Task:
        print(action.variables['value'])
        task = Task("t_quiz_hotite_rasskazat_pro_segodnya")
        return task

    def a_quiz_bolela_golova(self, action: Action, guid=None) -> Task:
        value = action.variables['value']
        if (value=="true"):
            task = Task("t_quiz_prinimali_obezbolivayushie")
        else: 
            task = Task("t_quiz_hotite_rasskazat_pro_segodnya")
        return task
    
    def a_quiz_hotite_rasskazat_pro_segodnya(self, action: Action, guid=None) -> Task:
        value = action.variables['value']
        if (value=="true"):
            task = Task("t_quiz_napishite_o_dne")
        else: 
            task = Task("t_quiz_final")
        return task

    def a_napishite_o_dne(self, action: Action, guid=None) -> Task:
        value = action.variables['value']
        task = Task("t_quiz_final")
        return task

    def a_doctor_toshnota_40(self, action: Action, guid=None) -> Task:
        task = Task("t_doctor_toshnota_40")
        task.add_data("var_doctor_toshnota", "true")       
        return task