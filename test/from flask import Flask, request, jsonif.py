from flask import Flask, request, jsonify, render_template, url_for, json, make_response
from flask_mail import Mail, Message
from pyairtable import Table
from pyairtable.formulas import match
from data import DataTest
import json
import static
import requests
import psycopg2
import gzip, sys
app = Flask(__name__)

@app.route('/get_tasks', methods=['POST'])
def get_tasks():
  tasks = Table(static.api_key, static.db, 'Task')
  tasks_all = tasks.all();
  t_list = []
  messages = Table(static.api_key, static.db, 'Messages')
  links = Table(static.api_key, static.db, 'Links')
  textfields = Table(static.api_key, static.db, 'TextField')

  for t in tasks_all:

      t_task = {}
      if 'Messages' in t['fields']:
          message_ids = t['fields']['Messages']
          message_id = message_ids[0]
          message = messages.get(message_id)['fields']
          t_message = {
              "text": message['Text'] if 'Text' in message else "",
              "en": message['En'] if 'En' in message else "",
              "text_variable": message['Text_variable'] if 'Text_variable' in message else ""
          }

      t_links = []
      if 'Links' in t['fields']:
        for link_id in t['fields']['Links']:
            link = links.get(link_id)['fields']
            actions_list = []
            for action_id in link['Actions']:
                action = links.get(action_id)['fields']
                actions_list.append({
                    "name": action['Name'] if 'Name' in action else "",
                    "type": action['Type'] if 'Type' in action else "",
                    "value": action['Value'] if 'Value' in action else "",
                })

            t_links.append({
                "name": link['Name'] if 'Name' in link else "",
                "text": link['Text'] if 'Text' in link else "",
                "en": link['En'] if 'En' in link else "",
                "value": link['Value'] if 'Value' in link else "",
                "actions": actions_list,
                # 'type': link['Type'] if 'Type' in link else "",
            })

      
      if 'TextField' in t['fields']:
          for textfield_id in t['fields']['TextField']:
              textfield = textfields.get(textfield_id)['fields']

              tf_actions_list = []
              for action_id in textfield['Actions']:
                  action = links.get(action_id)['fields']
                  tf_actions_list.append({
                      "name": action['Name'] if 'Name' in action else "",
                      "type": action['Type'] if 'Type' in action else "",
                      "value": action['Value'] if 'Value' in action else "",
                  })

              t_textfield = {
                  "name": textfield['Name'],
                  "validation": textfield['Validation'],
                  "text": textfield['Text'],
                  "en": textfield['En'],
                  "actions": tf_actions_list,
              }
              t_task['textfield'] =  t_textfield

      t_task['name'] =  t['fields']['Name']
      t_task['message'] =  t_message
      t_task['links'] =  t_links
      t_list.append(t_task)

  # pushTasksToPostgre(t_list)
  for tt in t_list:
    query=f"INSERT INTO tasks (name, task) VALUES ('{tt['name']}', '{json.dump(tt)}')"
    response=dbconnect_migrebot_query_select(query)
    # result=response[0][0]
    # resultJson=json.dumps(result,ensure_ascii=False, indent=4).encode('UTF-8')

  content = gzip.compress(json.dumps(t_list).encode('utf8'), 5)
  response = make_response(content)
  response.headers['Content-length'] = len(content)
  response.headers['Content-Encoding'] = 'gzip'
  response.headers['Content-Type'] = 'application/json'
  return response

# def pushTasksToPostgre(task_list):
#   for task in task_list:
#     query=f"INSERT INTO tasks (name, task) VALUES ('{task['name']}','{task}')"
#     response=dbconnect_migrebot_query_select(query)
#     result=response[0][0]
#     resultJson=json.dumps(result,ensure_ascii=False, indent=4).encode('UTF-8')
#   return resultJson

  


@app.route("/test")
def test():
  api_key = 'keyo3KIVE1eGZQFIv'
  db = 'app0DC4nEdzPuL20W'
  table = Table(api_key, db, 'Task')
  tab = table.all()
  content = gzip.compress(json.dumps(tab).encode('utf8'), 5)
  response = make_response(content)
  response.headers['Content-length'] = len(content)
  response.headers['Content-Encoding'] = 'gzip'
  return response
  # task='t_registration_check_email'
  # query=f"SELECT task FROM template_tasks"
  # response=dbconnect_migrebot_query_select(query)
  # result=response[0]
  # resultJson=json.dumps(result,ensure_ascii=False, indent=4).encode('UTF-8')
  # return resultJson

#   if jsonResponse["guid"]!='': # если guid присутствует
#     query=f"SELECT * FROM active_tasks WHERE guid='{jsonResponse['guid']}' AND active_set->>'email'='тут значение ключа'" 
#   else:
#     query=f"SELECT template_tasks FROM template_tasks"
    
#   response=dbconnect_migrebot_query_select(query)
#   result=response[0]
#   resultJson=json.dumps(result,ensure_ascii=False, indent=4 ).encode('UTF-8')
#   return resultJson

# # 
# # Команды
# # 
def action_initialize():
  task='t_registration_enter_email'
  query=f"SELECT task FROM template_tasks WHERE name='{task}'"
  response=dbconnect_migrebot_query_select(query)
  result=response[0][0]
  resultJson=json.dumps(result,ensure_ascii=False, indent=4).encode('UTF-8')
  return resultJson
  
  
def action_check_email():
  task='t_registration_check_email'
  query=f"SELECT task FROM template_tasks WHERE name='{task}'"
  response=dbconnect_migrebot_query_select(query)
  result=response[0][0]
  resultJson=json.dumps(result,ensure_ascii=False, indent=4).encode('UTF-8')
  return resultJson
  
def action_check_email_true():
  task='t_registration_check_email'
  query=f"SELECT task FROM template_tasks WHERE name='{task}'"
  response=dbconnect_migrebot_query_select(query)
  result=response[0][0]
  resultJson=json.dumps(result,ensure_ascii=False, indent=4).encode('UTF-8')
  return resultJson

# def action_send_email():
#   task='t_send_email'
#   query=f"SELECT task FROM template_tasks WHERE name='{task}'"
#   response=dbconnect_migrebot_query_select(query)
#   result=response[0][0]
#   resultJson=json.dumps(result,ensure_ascii=False, indent=4).encode('UTF-8')
#   return resultJson

# 
# Конец комманд
# 

##################################################### start your circle
#Получаем конкретного пользвоателя по его email
@app.route('/get_id_user_circle', methods=['GET'])
def get_id_user_circle():
  email=request.args.get('email')
  print(email)
  query=f"SELECT * FROM users WHERE email_user='{email}'"
  print(query)
  tek={}
  response=dbconnect_circle_query_select(query)
  print(response)
  if response!=[]:
    for item in response:
      tek={
        "id":item[0], 
        "guid_user":item[1], 
        "email_user":item[2], 
        "name":item[3]
      }
  # else:
  #   tek=None
  # print(tek)
  return jsonify(tek)


#Получем список пользователей
@app.route('/get_users_circle', methods=['GET'])
def get_users_circle():
  query=f'SELECT * FROM users'
  result=[]
  response=dbconnect_circle_query_select(query)
  if response!=[]:
    for item in response:
      tek={
        "id":item[0], 
        "guid_user":item[1], 
        "email_user":item[2], 
        "name":item[3]
      }
      result.append(tek)
  return jsonify(result)

#Получем список СТАТЕЙ пользователя
@app.route('/get_users_articles', methods=['GET'])
def get_users_articles():
  id=request.args.get('id')
  query=f"""SELECT id, article, url_article, guid_user, author_email, email_user,
   status_author, status_user, rem_user, favorites FROM articles WHERE id={int(id)}"""
  result=[]
  response=dbconnect_circle_query_select(query)
  if response!=[]:
    for item in response:
      tek={
        "id":item[0], 
        "article":item[1], 
        "url_article":item[2], 
        "guid_user":item[3], 
        "author_email":item[4], 
        "user_email":item[5], 
        "status_author":item[6], 
        "status_user":item[7], 
        "rem_user":item[8], 
        "favorites":item[9] 
      }
      result.append(tek)
  return jsonify(result)


def dbconnect_circle_query_select(query):
  conn = psycopg2.connect(database="ruitlab_tasks", user="postgres", password="##Doors321##", host="iibot.ru", port="5435")  # коннект к базе (локальной)
  cur = conn.cursor()
  cur.execute(query)
  response=cur.fetchall()
  cur = conn.close()
  conn.close()
  return response
##################################################### end your circle

@app.route("/")
def hello():
  return "<h1 style='color:blue'>Hello TetsBeck</h1>"


@app.route('/get_initialize_set', methods=['GET'])
def get_initialize_set(): # в body guid типа такой dict (map): { "guid" : ''} или { "guid" : 'jdsl44lkjlkjlkjfls;dv;'}
  response=request.data.decode('UTF-8') #Получаем body
  jsonResponse=json.loads(response, encoding="UTF-8")   # переводим в читабельный json
  
  if jsonResponse["guid"]!='': # если guid присутствует
    query=f"SELECT * FROM active_tasks WHERE guid='{jsonResponse['guid']}' AND active_set->>'email'='тут значение ключа'" 
  else:
    query=f"SELECT * FROM template_tasks"
    
  response=dbconnect_migrebot_query_select(query)
  result=response[0]
  resultJson=json.dumps(result,ensure_ascii=False, indent=4 ).encode('UTF-8')
  return resultJson


def dbconnect_migrebot_query_select(query):
  conn = psycopg2.connect(database="migrebot", user="postgres", password="##Doors321##", host="iibot.ru", port="5435")  # коннект к базе (локальной)
  cur = conn.cursor()
  cur.execute(query)
  response=cur.fetchall()
  cur = conn.close()
  conn.close()
  return response

# def dbconnect_migrebot():
#     conn = psycopg2.connect(database="migrebot", user="postgres", password="##Doors321##", host="iibot.ru", port="5435")  # коннект к базе (локальной)
#     return conn

def dbdisconnect(cur, conn):
  cur = conn.close()
  conn.close()


# @app.route('/get_initialize_set', methods=['GET'])
# def get_initialize_set(): 
  
#   result={
#     "name" : "s_initialize",
#     "commands": [
#       {
#         "order": 1,
#         "name": "c_email_registration",
#         "status": False
#       },
#       {
#         "order": 2,
#         "name": "c_select_locale",
#         "status": False
#       },
#       {
#         "order": 3,
#         "name": "c_set_notification",
#         "status": False
#       }, 
#     ]
#     }
#   resultJson=json.dumps(result,ensure_ascii=False, indent=4 ).encode('UTF-8')
#   return resultJson

# @app.route('/get_command?name=c_email_registration', methods=['GET'])
# def get_command(): 
#   command_name = request.args.get('name')
#   result={
#     "name" : "c_email_registration",
#     "tasks": [
#       {
#         "order": 1,
#         "name": "t_email_registration_start",
#         "status": False
#       },
#       {
#         "order": 2,
#         "name": "t_email_registration_start_send_email",
#         "status": False
#       },
#       {
#         "order": 3,
#         "name": "t_email_verification_enter",
#         "status": False
#       }, 
#     ]
#     }
#   resultJson=json.dumps(result,ensure_ascii=False, indent=4 ).encode('UTF-8')
#   return resultJson


@app.route('/beck/send_email', methods=['POST'])
def send_email(): #https://testbeck.ru/beck/send_email json внутри
  jsonstr=json.loads(request.data.decode('UTF-8'), encoding="UTF-8")
  jsonstr="dsdd"
  send_email(jsonstr)
  result={
    "message" : "Ваш отчет отправлен",
    "links" : [
          {
            "id":1,
            "message": "Отчет от 01 января 2021",
            "url": "https://testbeck.ru/report?guid=123"
          },
        ]
      }
  resultJson=json.dumps(result,ensure_ascii=False, indent=4 ).encode('UTF-8')
  return resultJson


#Этот метод получает из приложения Опрос и решистрирует его в базе,
#присваивая ему уникальный guid
#и в ответ отпралвяет ссылку на testbeck.ru/report?guid=1215jfenaskfn8n832nf
#Beck
@app.route('/beck/send_report', methods=['POST'])
def send_report(): #https://testbeck.ru/beck/send_report json внутри
  jsonstr=json.loads(request.data.decode('UTF-8'), encoding="UTF-8")
  result={
    "message" : "Все готово",
    "links" : [
        {
            "id":1,
            "message": "Результат",
            "url": "https://testbeck.ru/report?guid=123"
        },
        {
          "id":2,
          "message": "Помощь",
          "url": "https://testbeck.ru"
        }
      ]
    }
  resultJson=json.dumps(result,ensure_ascii=False, indent=4 ).encode('UTF-8')
  return resultJson

@app.route('/report', methods=['GET'])
def report():
  jsonData=DataTest.test_beck()
  # print(len(jsonData["items"]))
  #для отображения в две колонки, переделываем dict в 
  # result=[]
  # for item in jsonData["items"]:
  # jsonstr=json.loads(request.data.decode('UTF-8'), encoding="UTF-8")
  # resultJson=json.dumps(jsonstr,ensure_ascii=False, indent=4 ).encode('UTF-8')
  return render_template('report2.html', data=jsonData)

def send_email(msg_data): #
  app.config['MAIL_SERVER']='smtp.beget.com'
  app.config['MAIL_PORT'] = 2525
  app.config['MAIL_USERNAME'] = 'report@testbeck.ru'
  app.config['MAIL_PASSWORD'] = '##Befybdih123'
  app.config['MAIL_USE_TLS'] = True
  app.config['MAIL_USE_SSL'] = False
  app.config['MAIL_ASCII_ATTACHMENTS'] = True
  mail = Mail(app)
  msg = Message('Дневник', sender = 'report@testbeck.ru', recipients = ['vasiliy.rusakov@gmail.com', 'rualek@gmail.com'])
  msg.body=f"Ваш результат"
  mail.send(msg)

if __name__ == "__main__":
  app.run(host='0.0.0.0')
