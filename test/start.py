# from pyairtable import Table
# from flask import Flask, make_response, request
# import static
# import psycopg2
# import gzip
# import json

# import ActionsList


# actions_list = {
#     "a_initialize":  ActionsList.a_initialize,
#     "a_language_set": ActionsList.a_language_set,
#     "a_message_ura": ActionsList.a_message_ura,
#     "a_message_how_instructions": ActionsList.a_message_how_instructions,
#     "a_language_select": ActionsList.a_language_select,
#     "a_registration_email_enter": ActionsList.a_registration_email_enter,
#     "a_registration_email_check": ActionsList.a_registration_email_check,
#     "a_registration_enter_email_code": ActionsList.a_registration_enter_email_code,
#     "a_registration_email_check_code": ActionsList.a_registration_email_check_code,
#     "a_gender_select": ActionsList.a_gender_select,
#     "a_gender_set": ActionsList.a_gender_set,
#     "a_telegram_start": ActionsList.a_telegram_start,
#     "a_telegram_transfer": ActionsList.a_telegram_transfer,
#     "a_telegram_transfer_yes": ActionsList.a_telegram_transfer_yes,
# }
# app = Flask(__name__)

# # Экспорт Tasks из Airable в JSON Postgresql,
# # удаляя предварительно таблицу tasks
# @app.route('/update_tasks_db', methods=['POST'])
# def update_tasks_db():
#     tasks = Table(static.api_key, static.db, 'Task')
#     tasks_all = tasks.all()
#     t_list = []
#     messages = Table(static.api_key, static.db, 'Messages')
#     links = Table(static.api_key, static.db, 'Links')
#     textfields = Table(static.api_key, static.db, 'TextField')

#     for t in tasks_all:
#         t_task = {}
#         if 'Messages' in t['fields']:
#             message_ids = t['fields']['Messages']
#             message_id = message_ids[0]
#             message = messages.get(message_id)['fields']
#             t_message = {
#                 "text": message['Text'].replace("'", "`") if 'Text' in message else "",
#                 "en": message['En'].replace("'", "`") if 'En' in message else "",
#                 "text_variable": message['Text_variable'] if 'Text_variable' in message else ""
#             }
#             print(t_message)

#         t_links = []
#         if 'Links' in t['fields']:
#             for link_id in t['fields']['Links']:
#                 link = links.get(link_id)['fields']
#                 actions_list = []
#                 for action_id in link['Actions']:
#                     action = links.get(action_id)['fields']
#                     actions_list.append({
#                         "name": action['Name'] if 'Name' in action else "",
#                         "type": action['Type'] if 'Type' in action else "",
#                         "value": action['Value'] if 'Value' in action else "",
#                     })

#                 t_links.append({
#                     "name": link['Name'] if 'Name' in link else "",
#                     "text": link['Text'] if 'Text' in link else "",
#                     "en": link['En'] if 'En' in link else "",
#                     "value": link['Value'] if 'Value' in link else "",
#                     "actions": actions_list,
#                     # 'type': link['Type'] if 'Type' in link else "",
#                 })

#         if 'TextField' in t['fields']:
#             for textfield_id in t['fields']['TextField']:
#                 textfield = textfields.get(textfield_id)['fields']

#                 tf_actions_list = []
#                 for action_id in textfield['Actions']:
#                     action = links.get(action_id)['fields']
#                     tf_actions_list.append({
#                         "name": action['Name'] if 'Name' in action else "",
#                         "type": action['Type'] if 'Type' in action else "",
#                         "value": action['Value'] if 'Value' in action else "",
#                     })

#                 t_textfield = {
#                     "name": textfield['Name'],
#                     "validation": textfield['Validation'],
#                     "text": textfield['Text'],
#                     "en": textfield['En'],
#                     "actions": tf_actions_list,
#                 }
#                 t_task['textfield'] = t_textfield

#         t_task['name'] = t['fields']['Name']
#         t_task['message'] = t_message
#         t_task['links'] = t_links
#         t_list.append(t_task)

#     query_del = f"DELETE from tasks"
#     dbconnect_migrebot_query_truncate(query_del)
#     for tt in t_list:
#         query = f"INSERT INTO tasks VALUES ('{tt['name']}', '{json.dumps(tt)}')"
#         dbconnect_migrebot_query_update(query)

#     content = gzip.compress(json.dumps(t_list).encode('utf8'), 5)
#     response = make_response(content)
#     response.headers['Content-length'] = len(content)
#     response.headers['Content-Encoding'] = 'gzip'
#     response.headers['Content-Type'] = 'application/json; charset=utf-8'
#     return response

# # Все задачи из таблицы tasks
# @app.route("/get_tasks", methods=['GET'])
# def get_tasks():
#     query = f"SELECT name, task FROM tasks"
#     response = dbconnect_migrebot_query_select(query)
#     result = response
#     resultJson = json.dumps(result, ensure_ascii=False, indent=4).encode('UTF-8')
#     return resultJson

# # Удаляем все из таблицы tasks
# @app.route("/delete", methods=['GET'])
# def delete():
#     query = f"DELETE FROM tasks"
#     response = dbconnect_migrebot_query_select(query)
#     result = response
#     resultJson = json.dumps(result, ensure_ascii=False, indent=4).encode('UTF-8')
#     return resultJson


# # Принимает POST с Task из приложения
# # и запускает функцию
# # {
# #   "name": "t_registration_email_enter",
# #   "type": "server",
# #   "value": "",
# # }
# @app.route("/task", methods=['POST'])
# def task():
#     jsonbody = json.loads(request.data.decode('UTF-8'))
#     query = f"SELECT task FROM tasks WHERE name='{jsonbody['name']}'"
#     response = dbconnect_migrebot_query_select(query)
#     resultJson = json.dumps(response, ensure_ascii=False, indent=4).encode('UTF-8')
#     return resultJson


# # ------------------------------------------------------------------------------------------------ #
# #
# #       Ф У Н К Ц И И
# #       которые запускают пришедшие Actions
# #
# # ------------------------------------------------------------------------------------------------ #
# def a_initialize(value):
#   return a_language_select(value)

# def a_language_select(value):
#   return {"task":"t_language_select"}
  
# def a_language_set(value):
#   language = value
#   # TODO: изменить язык в профиле
#   return {
#       "task":"t_language_ok",
#       "text_variable": value
#       }

# def a_message_ura(value):
#   return {"task":"t_message_ura"}

# def a_message_how_instructions(value):
#   return {"task":"t_message_how_instructions"}

# # ------------------------------------------------------------------------------------------------ #
# #
# #       A C T I O N S
# #       действия, которые приходят от приложения
# #
# # ------------------------------------------------------------------------------------------------ #
# actions_list = {
#     "a_initialize": a_initialize,
#     "a_language_set": a_language_set,
#     "a_message_ura": a_message_ura,
#     "a_message_how_instructions": a_message_how_instructions,
#     "a_language_select": a_language_select,
# }

# # Сюда из app приходит {
# #   "name": "a_registration_email_enter",
# #   "type": "server",
# #   "value": "",
# # }
# @app.route("/action", methods=['POST'])
# def action():
#   jsonbody = json.loads(request.data.decode('UTF-8'))
  
#   action_name = jsonbody['name'] if 'name' in jsonbody else ""
#   action_value = jsonbody['value'] if 'value' in jsonbody else ""

#   print(f"Action: {action_name} и его value: {action_value}")

#   if action_name in actions_list:
#     action = actions_list.get(action_name)
#     result = action(action_value)
#     task_name = result['task'] if 'task' in result else ""
#     text_variable = result['text_variable'] if 'text_variable' in result else ""
#     taskObj = get_task_object(task_name)
#     taskObj = filter_text_variable(taskObj, text_variable)
#   else:
#     taskObj = {"message": {"text": f"ERROR: Action {action_name} does not exist!"}}
#   resultJson = json.dumps(taskObj, ensure_ascii=False, indent=4).encode('utf-8')
#   return resultJson


# # ------------------------------------------------ #
# # Вспомогательные функции
# # ------------------------------------------------ #

# def get_task_object(name):
#   if name != "":
#     query = f"SELECT task FROM tasks WHERE name='{name}'"
#     response = dbconnect_migrebot_query_select(query)
#     return response[0][0]
#   else:
#     return {}

# def filter_text_variable(taskObj, text_variable):
#   taskObj['message']['text_variable'] = text_variable
#   taskObj['message']['text'] = taskObj['message']['text'].replace('#text_variable', text_variable)
#   taskObj['message']['text'] = taskObj['message']['text'].replace('\\n', '\u000A')
#   return taskObj

# # ------------------------------------------------ #
# # БД
# # ------------------------------------------------ #

# def dbconnect_migrebot_query_select(query):
#     conn = db_connect()
#     cur = conn.cursor()
#     cur.execute(query)
#     response = cur.fetchall()
#     cur.close()
#     conn.close()
#     return response


# def dbconnect_migrebot_query_update(query):
#     conn = db_connect()
#     cur = conn.cursor()
#     cur.execute(query)
#     conn.commit()
#     cur.close()
#     conn.close()


# def dbconnect_migrebot_query_truncate(query):
#     conn = db_connect()
#     cur = conn.cursor()
#     cur.execute(query)
#     conn.commit()
#     cur.close()
#     conn.close()


# def db_connect():
#     return psycopg2.connect(database="migrebot", user="postgres", password="##Doors321##", host="iibot.ru", port="5435")


# if __name__ == "__main__":
#     app.run(host='0.0.0.0')