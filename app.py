from flask import Flask, request
from actions import ActionsList
from data.airtable import airtable_to_postgresql_update, commands_airtable_to_postgresql_update
from models.action import Action
from models.task import Task
import json
from data.db import DB

app = Flask(__name__)
actions_list = ActionsList(app)

@app.route('/update_tasks_db')
def update_tasks_db():
    response = airtable_to_postgresql_update()
    return response

@app.route('/update_commands_db')
def update_commands_db():
    response = commands_airtable_to_postgresql_update()
    return response


@app.route("/get_tasks", methods=['GET'])
def get_tasks():
    query = f"SELECT name, task FROM tasks"
    response = DB.query_select(query)
    result = response
    resultJson = json.dumps(result, ensure_ascii=False, indent=4).encode('UTF-8')
    return resultJson


# Выводит список всех комманд
@app.route("/commands", methods=['GET'])
def commands():
    guid = request.args.get('guid')
    # TODO выдавать в зависимости от guid и статуса юзера 
    query = f"SELECT name, data, action FROM commands"
    response = DB.query_select(query)
    commands_list = [] 

    for command in response:
        commands_list.append({
            "name": command[0],
            "data": command[1]
        })

    if any(response):
        resultJson = json.dumps(commands_list, ensure_ascii=False).encode('UTF-8')
        return resultJson
    else:
        return ""

# Сюда из app приходит {
#   "name": "a_registration_email_enter",
#   "type": "server",
#   "value": "",
# }


@app.route("/action", methods=['GET', 'POST'])
def action():
    guid = request.args.get('guid')
    jsonbody = json.loads(request.data.decode('UTF-8'))
    action = Action(name=jsonbody['name'], variables=jsonbody['variables'])
    # print(jsonbody)
    # Ищем метод в классе ActionsList по названию
    action_method = getattr(actions_list, action.name, None)
    result = action_method(action, guid) if action_method != None else None
    
    if type(result) is Task:
        task = get_task_from_db(result)

        if task!=None and task!="":
            # Возможно нужно удалить
            if result.variable != None:
                task['variable'] = result.variable.__dict__

            # Дополнительные переменные в таск (add_data)
            if len(result.data) > 0:
                for data in result.data:
                    task['data'].append(data)

            # Дополнительные кнопки в таск (например таблетки)                
            if len(result.links) > 0:
                for link in result.links:
                    task['links'].append(link)
            task = prepare_task_object(task)
        else:
            task = {"message": {"text": f"[server]: Action {action.name} does not exist!"}}
    else:
        task = {"message": {"text": f"[server]: Action {action.name} does not exist!"}}
    response = json.dumps(task, ensure_ascii=False, indent=4).encode('utf-8')
    return response

# Принимает POST с Task из приложения
# и запускает функцию
# {
#   "name": "t_registration_email_enter",
#   "type": "server",
#   "value": "",
# }
@app.route("/task", methods=['POST'])
def task():
    jsonbody = json.loads(request.data.decode('UTF-8'))
    query = f"SELECT task FROM tasks WHERE name='{jsonbody['name']}'"
    response = DB.query_select(query)
    if any(response):
        resultJson = json.dumps(response[0][0], ensure_ascii=False, indent=4).encode('UTF-8')
        return resultJson
    else:
        return ""

# POST: {"email": "rualek@gmail.com"}
@app.route("/get_guid", methods=['POST'])
def get_guid():
    jsonbody = json.loads(request.data.decode('UTF-8'))
    query = f"SELECT guid FROM profiles WHERE profile ->> 'email' = '{jsonbody['email']}'"
    response = DB.query_select(query)
    if any(response):
        resultJson = json.dumps(response[0][0], ensure_ascii=False).encode('UTF-8')
        return resultJson
    else:
        return ""


# ------------------------------------------------ #
# Вспомогательные функции
# ------------------------------------------------ #

def get_task_from_db(task=None):
    if task != None:
        query = f"SELECT task FROM tasks WHERE name='{task.name}'"
        response = DB.query_select(query)
        if len(response):
            if any(response[0]):
                result = response[0][0]
                return result


def prepare_task_object(taskObj):
    taskObj['message']['text'] = taskObj['message']['text'].replace('\\n', '\u000A')
    lang = {}
    for name in taskObj['message']['lang']:
        lang[name] = taskObj['message']['lang'][name].replace('\\n', '\u000A')
    taskObj['message']['lang'] = lang
    return taskObj


if __name__ == "__main__":
    app.debug = True
    app.run()
    # flask run --host=192.168.0.10
