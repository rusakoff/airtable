import psycopg2

class DB:

    def db_connect():
        return psycopg2.connect(database="migrebot", user="postgres", password="##Doors321##", host="iibot.ru", port="5435")
    
    def query_select(query):
        conn = DB.db_connect()
        cur = conn.cursor()
        cur.execute(query)
        response = cur.fetchall()
        cur.close()
        conn.close()
        return response


    def query_update(query):
        conn = DB.db_connect()
        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        cur.close()
        conn.close()


    def delete_table(tablename):
        query_del = f"DELETE from {tablename}"
        conn = DB.db_connect()
        cur = conn.cursor()
        cur.execute(query_del)
        conn.commit()
        cur.close()
        conn.close()
    
    

    