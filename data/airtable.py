from pyairtable import Table
from data.db import DB
import static, json, gzip
from flask import make_response


def commands_airtable_to_postgresql_update():
    commands = Table(static.api_key, static.db, 'Commands').all()
    actions = Table(static.api_key, static.db, 'Actions')
    variables = Table(static.api_key, static.db, 'Variables')
    # print(commands)
    c_list = []
    for command in commands:
        c_item = {
            "name": command['fields']['Name'],
            "data": command['fields']
            }
        if 'Actions' in command['fields']:
            actions_list=[]
            for action_id in command['fields']['Actions']:
                action = actions.get(action_id)['fields']
                set_variable = ""
                if 'set_variable' in action:
                    variable_id=action['set_variable'][0]
                    set_variable = variables.get(variable_id)['fields']['Name']
                    # print(set_variable)
                
                get_variables = []
                if 'get_variables' in action:
                    for variable_id in action['get_variables']:
                        variable = variables.get(variable_id)['fields']['Name']
                        get_variables.append(variable)
                
                # print(get_variables)

                actions_list.append({
                    "name": action['Name'] if 'Name' in action else "",
                    "type": action['Type'] if 'Type' in action else "server",
                    "value": action['Value'] if 'Value' in action else "",
                    "set_variable": set_variable,
                    "get_variables": get_variables,
                })
                # c_item['action']=actions.get(action_id)['fields']['Name']
                c_item['data']['actions']=actions_list

        c_item['data']['lang'] = {
            "ru": command['fields']['Ru'] if 'Ru' in command['fields'] else "",
            "en": command['fields']['En'] if 'En' in command['fields'] else "",
        }
        if 'Actions' in c_item['data']: del c_item['data']['Actions']
        if 'Ru' in c_item['data']: del c_item['data']['Ru']
        if 'En' in c_item['data']: del c_item['data']['En']
        c_item['data'] = {k.lower(): v for k, v in c_item['data'].items()}
        c_list.append(c_item)

    DB.delete_table('commands')
    for cc in c_list:
        # if 'action' in cc:
        query = f"INSERT INTO commands (name, data) VALUES ('{cc['name']}', '{json.dumps(cc['data'])}')"
        DB.query_update(query)
    content = gzip.compress(json.dumps(c_list).encode('utf8'), 5)
    response = make_response(content)
    return response



def airtable_to_postgresql_update():
    tasks = Table(static.api_key, static.db, 'Task')
    tasks_all = tasks.all()
    t_list = []
    messages = Table(static.api_key, static.db, 'Messages')
    links = Table(static.api_key, static.db, 'Links')
    textfields = Table(static.api_key, static.db, 'TextField')
    actions = Table(static.api_key, static.db, 'Actions')
    variables = Table(static.api_key, static.db, 'Variables')
    validations = Table(static.api_key, static.db, 'Validations')
    
    for t in tasks_all:
        t_task = {}
        # if 'Variables' in t['fields']:
        #     variable_ids = t['fields']['Variables']
        #     variable_id = variable_ids[0]
        #     variable = variables.get(variable_id)['fields']
        #     t_variable = {
        #         "name": variable['Name'] if 'Name' in variable else "",
        #         "value": variable['Value'] if 'Value' in variable else "",
        #     }
        #     t_task['variable'] = t_variable
        if not 'Name' in t['fields']:
            continue



        t_data = []
        if 'Data' in t['fields']:
            variable_ids = t['fields']['Data']
            for variable_id in variable_ids:
                
                variable = variables.get(variable_id)['fields']
                t_variable = {
                    "name": variable['Name'] if 'Name' in variable else "",
                    "value": variable['Value'] if 'Value' in variable else "",
                }
                t_data.append(t_variable)

        if 'Messages' in t['fields']:
            message_ids = t['fields']['Messages']
            message_id = message_ids[0]
            message = messages.get(message_id)['fields']
            t_message = {
                "text": message['Text'].replace("'", "`") if 'Text' in message else "",
                "lang": {
                    "ru": message['Text'].replace("'", "`") if 'Text' in message else "",
                    "en": message['En'].replace("'", "`") if 'En' in message else "",
                },
            }
            t_task['message'] = t_message

        t_links = []
        if 'Links' in t['fields']:
            for link_id in t['fields']['Links']:
                link = links.get(link_id)['fields']
                actions_list = []
                for action_id in link['Actions']:
                    action = actions.get(action_id)['fields']

                    set_variable = ""
                    if 'set_variable' in action:
                        variable_id=action['set_variable'][0]
                        set_variable = variables.get(variable_id)['fields']['Name']
                        # print(set_variable)
                    
                    get_variables = []
                    if 'get_variables' in action:
                        for variable_id in action['get_variables']:
                            variable = variables.get(variable_id)['fields']['Name']
                            get_variables.append(variable)
                    
                    # print(get_variables)

                    actions_list.append({
                        "name": action['Name'] if 'Name' in action else "",
                        "type": action['Type'] if 'Type' in action else "server",
                        "value": action['Value'] if 'Value' in action else "",
                        "set_variable": set_variable,
                        "get_variables": get_variables,
                    })
                    
                    # print(action['set_variable'] if 'set_variable' in action else "" + "<-set  get-> " + action['get_variables'] if 'get_variables' in action else "")

                t_links.append({
                    "name": link['Name'] if 'Name' in link else "",
                    "text": link['Text'] if 'Text' in link else "",
                    "lang": {
                        "ru": link['Text'] if 'Text' in link else "",
                        "en": link['En'] if 'En' in link else "",
                    },
                    "value": link['Value'] if 'Value' in link else "",
                    "actions": actions_list,
                    # 'type': link['Type'] if 'Type' in link else "",
                })

        if 'TextField' in t['fields']:
            for textfield_id in t['fields']['TextField']:
                textfield = textfields.get(textfield_id)['fields']

                tf_actions_list = []
                for action_id in textfield['Actions']:
                    action = actions.get(action_id)['fields']

                    set_variable = ""
                    if 'set_variable' in action:
                        variable_id=action['set_variable'][0]
                        set_variable = variables.get(variable_id)['fields']['Name']
                        # print(set_variable)
                    
                    get_variables = []
                    if 'get_variables' in action:
                        for variable_id in action['get_variables']:
                            variable = variables.get(variable_id)['fields']['Name']
                            get_variables.append(variable)

                    tf_actions_list.append({
                        "name": action['Name'] if 'Name' in action else "",
                        "type": action['Type'] if 'Type' in action else "",
                        "value": action['Value'] if 'Value' in action else "",
                        "set_variable": set_variable,
                        "get_variables": get_variables,
                    })

                t_validation = ""
                if 'Validation' in textfield:
                    for validation_id in textfield['Validation']:
                        validation = validations.get(validation_id)['fields']
                        t_validation = validation['Name'] if 'Name' in validation else ""

                t_textfield = {
                    "name": textfield['Name'] if 'Name' in textfield else "",
                    "validation": t_validation,
                    "text": textfield['Text'] if 'Text' in textfield else "",
                    "lang": {
                        "ru": textfield['Text'] if 'Text' in textfield else "",
                        "en": textfield['En'] if 'En' in textfield else "",
                    },
                    "actions": tf_actions_list,
                }
                t_task['textfield'] = t_textfield

        t_task['name'] = t['fields']['Name']
        t_task['links'] = t_links
        t_task['data'] = t_data
        t_list.append(t_task)

    
    DB.delete_table('tasks')

    for tt in t_list:
        query = f"INSERT INTO tasks VALUES ('{tt['name']}', '{json.dumps(tt)}')"
        DB.query_update(query)

    content = gzip.compress(json.dumps(t_list).encode('utf8'), 5)
    response = make_response(content)
    response.headers['Content-length'] = len(content)
    response.headers['Content-Encoding'] = 'gzip'
    response.headers['Content-Type'] = 'application/json; charset=utf-8'
    return response
